<?php
/**
 * @file
 * uw_lib_assignment_planner.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_lib_assignment_planner_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create assignment_planner content'.
  $permissions['create assignment_planner content'] = array(
    'name' => 'create assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any assignment_planner content'.
  $permissions['delete any assignment_planner content'] = array(
    'name' => 'delete any assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own assignment_planner content'.
  $permissions['delete own assignment_planner content'] = array(
    'name' => 'delete own assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any assignment_planner content'.
  $permissions['edit any assignment_planner content'] = array(
    'name' => 'edit any assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own assignment_planner content'.
  $permissions['edit own assignment_planner content'] = array(
    'name' => 'edit own assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter assignment_planner revision log entry'.
  $permissions['enter assignment_planner revision log entry'] = array(
    'name' => 'enter assignment_planner revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner authored by option'.
  $permissions['override assignment_planner authored by option'] = array(
    'name' => 'override assignment_planner authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner authored on option'.
  $permissions['override assignment_planner authored on option'] = array(
    'name' => 'override assignment_planner authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner promote to front page option'.
  $permissions['override assignment_planner promote to front page option'] = array(
    'name' => 'override assignment_planner promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner published option'.
  $permissions['override assignment_planner published option'] = array(
    'name' => 'override assignment_planner published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner revision option'.
  $permissions['override assignment_planner revision option'] = array(
    'name' => 'override assignment_planner revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override assignment_planner sticky option'.
  $permissions['override assignment_planner sticky option'] = array(
    'name' => 'override assignment_planner sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search assignment_planner content'.
  $permissions['search assignment_planner content'] = array(
    'name' => 'search assignment_planner content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'search_config',
  );

  return $permissions;
}
