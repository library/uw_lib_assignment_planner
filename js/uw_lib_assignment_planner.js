/**
 * @file
 * This file contains the script for accordian steps and printing
 * The purpose of this planner is to help students break down their assignment or project into manageable steps, direct students to useful guides & services such as Writing Centres
 * Written by Lathangi Ganesh
 * Jan 23, 2018
 */

(function ($) {
  $(document).ready(function() {

    $url = window.location.href;
    var url_param = $url.split("/");
    var url_elm = url_param .indexOf("assignment-planner");
    var index_val = url_param[url_elm];
    var con_type = "assignment-planner";

    if(index_val == con_type)
    {
      $("#edit-start-date-datepicker-popup-0").attr("autocomplete", "off");
      $("#edit-end-date-datepicker-popup-0").attr("autocomplete", "off");
      //Renaming date to Start Date and End Date
      $("label[for='edit-start-date-datepicker-popup-0']").text("Begin Assignment");
      $("label[for='edit-end-date-datepicker-popup-0']").text("Assignment Due");
      if ($("#edit-start-date").length > 0) {
        var tag = document.getElementById("edit-start-date").parentElement.nodeName;
        tag = tag.toLowerCase();
        if(tag == "fieldset") {
          $("#edit-start-date").unwrap();
          $("#edit-end-date").unwrap();
        }
      }

      $("#expand_collapse_all").click(function() {
        if($('.all_detail').attr('open')) {
          $("#expand_collapse_all").text("Expand All Steps (+)");
          $('.all_detail').removeAttr("open");
        }else {
          $("#expand_collapse_all").text("Collapse All Steps (-)");
          $('.all_detail').attr('open', '');
        }
        return false;
      });

      //Print button
      $("#prnt_btn").click(function(){
        var doc = new jsPDF();
        var elementHandler = {
          '#ignorePDF': function (element, renderer) {
            return true;
          }
        };

        //Get the right contents for printing
        var page = $("body").html();
        var tit = $('.uw-site--title', $(page)).remove().html();
        var title = "<div style='text-align:center;'>" + tit + "</div>";
        var desc = $('.assign_desc', $(page)).remove().html() + "<br>";
        var msg = $('.top_msg', $(page)).remove().html() + "<br>";
        var step_tit = "<h2>Your steps</h2><br>";
        var steps = $('.all_steps', $(page)).remove().html() + "<br>";

        if(msg.includes("null")) {
          msg = "";
        }
        var source_pdf = title + desc + step_tit + steps;
        doc.fromHTML(
        source_pdf,
        10,
        10,
        {
          'width': 180,'elementHandlers': elementHandler
        });

        //This is working for chrome and other browsers but not with edge
        var string = doc.output('datauristring');
        var iframe = "<iframe width='100%' height='100%' src='" + string + "'></iframe>"
        var x = window.open();
        x.document.open();
        x.document.write(iframe);
        x.document.close();
      });

      //Expand or collapse steps
      var detailsElem = document.querySelector('details');
      detailsElem.addEventListener("toggle", function(evt) {
        if (detailsElem.open) {
          /* the element was toggled open */
        } else {
          /* the element was toggled closed */
        }
      }, false);
    } //End of checking url

  });
})(jQuery);
