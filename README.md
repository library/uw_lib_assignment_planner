INTRODUCTION
------------
Library Assignment Planner(uw_lib_assign_planner)
This module provides Assignment Planner by the UW Library.
The purpose of this planner is to help students break down an assignment or project into manageable steps, direct students to useful guides & services

INSTALLATION
------------
- Clone the module into the respective /modules folder on the site
- Enable the module as usual
- The source code is available at https://git.uwaterloo.ca/library/uw_lib_assignment_planner

CONFIGURATION
-------------
- None required.

PERMISSIONS
-----------
Node type: Assignment Planner
- Create new content: content editor, site manager, administrator, WCMS support
- Edit own content: content editor, site manager, administrator, WCMS support
- Edit any content: content editor, site manager, administrator, WCMS support
- Delete own content: content editor, site manager, administrator, WCMS support
- Delete any content: site manager

MAINTAINER(S)
-------------
The module is created and maintained by Lathangi Ganesh(l4ganesh@uwaterloo.ca) from the Digital Initiatives department of the UWaterloo Library.
