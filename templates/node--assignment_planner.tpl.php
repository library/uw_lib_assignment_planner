<?php
/**
 * @file
 * This a template for the content type assignment planner
 * Written by Lathangi Ganesh
 * Jan 23, 2018
 */
$page_form = "";
$page = "";
$top_buttons = "";
$top_msg = "";
$tot = 0;
$google_cal = "";

$default = FALSE;
if(!isset($_SESSION['assign_flag'])) {
  $default = TRUE;
}

if(!empty($content['assignment_description'])) {
  //Hide comments from the node
  hide($content['comments']);

  $obj = $content['assignment_description']['#object'];

  $page_form .= "<div class='assign_desc'>" . $obj->assignment_description['und'][0]['safe_value'] . "</div>";
  if(isset($obj->upload_file_desc['und'][0]['safe_value']) && !empty($obj->upload_file_desc['und'][0]['safe_value'])) {
    $upload_file_desc = $obj->upload_file_desc['und'][0]['safe_value'];
  }
  $title = $obj->title;

  $assignment_planner_form = drupal_get_form('uw_lib_assignment_planner_form');
  $assignment_planner_form = drupal_render($assignment_planner_form);
  $page_form .= "<div class ='cal_form'>" . $assignment_planner_form . "</div>";
  if( (isset($_SESSION['assign_flag']) && $_SESSION['assign_flag']) || $default) {
    $items = array();
    $nid = $obj->nid;
    $vid = $obj->vid;
    $node = node_load($nid, $vid);

    $steps_items = field_get_items('node', $node, 'steps');
    $max = count($steps_items);
    for($x = 0; $x <= $max - 1; $x++) {
      $items[] = $steps_items [$x] ['value'];
    }
    //Looping through fieldcollection items
    if(!empty($items)) {
      $fc_items = entity_load('field_collection_item',$items);
      if(isset($_SESSION['start_date']) && !empty($_SESSION['start_date'])) {
        $start = $_SESSION['start_date'];
      }
      else {
        $start = "";
      }
      $time = 0;
      $i = 1;

      //Displaying uploaded files
      if(isset($upload_file_desc) && !empty($upload_file_desc)) {
        $page .=  "<div class='assign_file_upload_desc'>" . $upload_file_desc . "</div>";
      }
      if(isset($obj->resource_files['und']) && !empty($obj->resource_files['und'])) {
        $res_files = $obj->resource_files['und'];
      }

      if(isset($res_files) && !empty($res_files)) {
        $files = "";
        $page .= "<div class='files_wrap'>";
        $page .= "<ul>";
        $file_types = array("jpg", "png", "gif");
        foreach($res_files as $file) {
          $fid = $file['fid'];
          $uri = $file['uri'];
          $file_name = $file['filename'];
          $tmp = explode(".",$file_name);
          $ext = strtolower($tmp[1]);
          $file_load = file_load($fid);
          $url = $file_load->uri;
          $file_url = file_create_url($url);
          $page .= '<li class="resrc_wrap">';
          if(in_array($ext, $file_types)) {
            $new_fname = $tmp[0].".".$ext;
            $alt = str_replace("_"," ",$tmp[0]);
            $alt = str_replace("-"," ",$alt);
            $alt = ucfirst($alt);
            $page .=  '<a class="resrc" target="_blank" href="'.$file_url.'"><img class="resrc resrcimg" src="'.$file_url.'" width="80" height="80" alt="'.$alt.'"><div class="resrc_img">'. $new_fname . '</div></a>';
          }
          else {
            $page .=  '<a class="resrc" target="_blank" href="'.$file_url.'"><div class="resrc_name">';
            $page .= $file_name . '</div></a>';
          }
          $page .= "</li>";
        }
      }
      $page .= "</ul>";
      $page .= "</div>";

      //$top_buttons =  '<span class="print"><input type="button" id="prnt_btn" value="PDF of this page"></span>';
      $hid_arr = array();

      if(isset($_SESSION['start_date']) && !empty($_SESSION['start_date'])) {
        $hid_arr['bdate'] = strtotime($_SESSION['start_date']);
        $hid_arr['edate'] = strtotime($_SESSION['due_date']);
        $hid_arr['title'] = $title;
      }
      $gc_steps = array();

      //Assignment planner steps
      $page .= "<h2>Your steps</h2>";
      $page .= "<span class='instruction'>Click on the steps to expand or collapse.</span>";
      $page .= "<span class=\"ex_all\"><a href=\"\" title=\"Expand/Collapse\" id=\"expand_collapse_all\">Expand All Steps (+)</a></span>";
      $page .= "<div class='all_steps'>";
      foreach($fc_items as $fc_item) {
        $percent =  (int)$fc_item->step_percent['und'][0]['value'];
        $time = calculate_date_steps($percent, $time);
        $r = (int)$time;
        $t = date('D M d, Y', strtotime($start. ' + ' . $r . ' days'));
        if(!empty($_SESSION['results'])) {
          $d = "Complete by " . $t;
        }
        else {
          $d = $i;
        }

        //if there is no date then percentage only
        if(strlen($d) > 1) {
          $d = $d;
        }
        else {
          $d = "";
        }

        if(!empty($d)) {
          $doby = "  <span class='doby'>". $d . "</span>";
        }
        else {
          $doby = "";
        }
        $page .= "<details class='all_detail'>";
        $page .= "<summary><h3 class='step_title'>Step ". $i . ": " . $fc_item->step_title['und'][0]['value'] . $doby . "</h3></summary>";
        $page .= "<div class='assign_percent'>Percent time spent on this step: " . trim($fc_item->step_percent['und'][0]['value']) . "%</div>";
        $step_date = "step".$i;
        $gc_steps[$step_date] = "Step".$i." ".$fc_item->step_title['und'][0]['value']." ".$d;
        if(isset($fc_item->step_resources['und'][0]['value']) && !empty($fc_item->step_resources['und'][0]['value'])) {
          $page .=  "<div class='step_content'>" . ltrim($fc_item->step_resources['und'][0]['value']) . "</div>";
        }
        $page .= "</details>"; //End of each step

        $time = $time;
        $i++;
        $tot = $tot + $percent;
      }//End of loop

      $page .= "</div>"; //End of all steps

      $hid_arr['gc_steps'] = $gc_steps;
      $_SESSION['gc_data'] = $hid_arr;

      global $base_url;
      $module_path = drupal_get_path('module', 'uw_lib_assignment_planner');
      $add_to_calendar_path = $base_url . "/" . $module_path;

      if(isset($_SESSION['gc_data']['bdate']) && !empty($_SESSION['gc_data']['bdate'])) {
        $top_buttons .= '<span class="ac_google_cal"><a href="' . add_to_google_calendar("", "", "", "University of Waterloo", "detail") . '" target="_blank" title="Add to Google Calendar">';
        $top_buttons .= '<img border="0" alt="Add to Google Calendar" src="'. $add_to_calendar_path .'/css/images/add_to_calendar.png" width="50" height="50"></a></span>';
      }
    }
    if($tot == 100) {
      if(isset($_SESSION['gc_data']['bdate']) && !empty($_SESSION['gc_data']['bdate'])) {
        $page_top = "<div class='page_top'>".$top_buttons."</div>";
      }else{
        $page_top = "";
      }

      if(isset($_SESSION['start_date']) && !empty($_SESSION['start_date'])) {
        $top_msg .= "<div class='top_msg'>";
        $top_msg .=  "<p>According to your start and end dates ( <strong>". $_SESSION['start_date'] . "</strong> to <strong>". $_SESSION['due_date'] ."</strong> ), <br> you have <strong>". (int) $_SESSION['results'] . "</strong> days to finish your assignment.</p>";
        $top_msg .= $top_buttons;
        $top_msg .= "</div>";
      }

      $page = $page_form.$top_msg.$page;
    }
    else {
      $page =  "<span class='assign_msg'>Total percentage must be 100% to get accurate results. <br> Please revise the steps and percentage for this assignment type</span>";
      $page .= $page . $page_form;
    }
  }
  else {
    $page .= "<span class='assign_msg'>No valid date range given for calculation</span>";
    $page .= $page . $page_form;
  }
  echo $page;
  $_SESSION['assign_flag'] = TRUE;
  unset_session_vars();
}

//Calculate due date for each steps
function calculate_date_steps($percent, $time) {
  if(isset($_SESSION['results'])) {
    $days = $_SESSION['results'];
    $num = $percent/100 * intval($days);
    $tot = $num + $time;
    return $tot;
  }
}

//Add to google callendar
function add_to_google_calendar($title='', $startdate='', $enddate='', $location='', $details='') {
  if(isset($_SESSION['gc_data']) && !empty($_SESSION['gc_data'])) {
    $title = $_SESSION['gc_data']['title'];
    $startdate = $_SESSION['gc_data']['bdate'];
    $enddate = $_SESSION['gc_data']['edate'];
    $sdate = $startdate + 86400;
    $edate = $enddate + 86400;
    $details = "";
    $details = "Steps and due dates <br>";
    foreach($_SESSION['gc_data']['gc_steps'] as $step) {
      $details .= $step."<br>";
    }
    $google_url = "http://www.google.com/calendar/event";
    $action = "?action=TEMPLATE";
    $title = ( $title ? ("&text=" . urlencode($title)) : "") ;
    $dates = "&dates=" . get_ical_date($sdate) . "Z/" . get_ical_date($edate) . "Z";
    $location = ( $location ? ("&location=" . urlencode($location)) : "") ;
    $details = ( $details ? ("&details=" . urlencode($details)) : "") ;
    $out = $google_url . $action . $title . $dates . $location . $details;
    return $out;
  }
}

function get_ical_date($time, $incl_time = true) {
  return $incl_time ? date('Ymd\THis', $time) : date('Ymd', $time);
}

/**
 * To unset session variable
 */
function unset_session_vars() {
  $_SESSION['start_date'] = "";
  $_SESSION['due_date'] = "";
  $_SESSION['results'] = "";
  unset($_SESSION['gc_data']);
}
