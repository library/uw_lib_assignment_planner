<?php
/**
 * @file
 * This file create the content type assignment planner
 * Written by Lathangi Ganesh
 * Jan 23, 2018
 */

/**
 * Implements hook_install().
 */
function uw_lib_assignment_planner_install() {
  $t = get_t();

  //Define the custom content type Assignment Planner
  $content_type = array(
    'type'          => 'assignment_planner',
    'name'          => $t('Assignment Planner'),
    'description' => $t('Create a Assignment Template'),
    'title_label' => $t('Assignment template title'),
    'base'          => 'node_content',
    'custom'        => TRUE,
  );

  variable_set('node_options_assignment_planner', array('revision', 'moderation'));
  $node_type = node_type_set_defaults($content_type);

  node_type_save($node_type);

  // Create all the fields we are adding to our entity type.
  // http://api.drupal.org/api/function/field_create_field/7
  foreach (_assignment_planner_field_data() as $field) {
    field_create_field($field);
  }

  // Create all the instances for our fields.
  // http://api.drupal.org/api/function/field_create_instance/7
  foreach (_assignment_planner_instance_data($t) as $instance) {
    $instance['entity_type']   = 'node';
    $instance['bundle']        = 'assignment_planner'; // Attach the instance to our content type
    field_create_instance($instance);
  }

  //To create field collection fiels
  _assignment_planner_create_field_collections_fields();
}

/**
 * Create the array of information about the fields we want to create.
 */
function _assignment_planner_field_data() {
  $fields = array(
    // Description
    'assignment_description'  => array(
      'field_name'  => 'assignment_description',
      'type'          => 'text_long',
      'cardinality' => 1,
    ),

     //Uploaded files description
    'upload_file_desc'  => array(
      'field_name'  => 'upload_file_desc',
      'type'          => 'text_long',
      'cardinality' => 1,
    ),

    //Resource files
    'resource_files'  => array(
      'field_name'  => 'resource_files',
      'type'          => 'file',
      'cardinality' => 7,
      'translatable' => FALSE,
    ),

      //Hidden Resource files
    'hidden_resource_files'  => array(
      'field_name'  => 'hidden_resource_files',
      'type'          => 'file',
      'cardinality' => -1,
      'translatable' => FALSE,
    ),

    //Steps
    'steps'  => array(
      'field_name'  => 'steps',
      'type'          => 'field_collection',
      'cardinality' => -1,
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    ),

  );
  return $fields;
}

/**
 * Create the array of information about the instances we want to create.
 */
function _assignment_planner_instance_data($t) {
  $instances = array(

    // Description / Body
    'assignment_description'  => array(
      'field_name'   => 'assignment_description',
      'label'        => $t('Description'),
      'required'     => TRUE,
      'widget'       => array(
        'type'  => 'text_textarea'
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
    ),

    //Uploaded files descriptions
    'upload_file_desc'  => array(
      'field_name'   => 'upload_file_desc',
      'label'        => $t('Uploaded files description'),
      'required'     => FALSE,
      'widget'       => array(
        'type'  => 'text_textarea'
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
    ),

     //Resources files
    'resource_files'  => array(
      'field_name'   => 'resource_files',
      'label'        => $t('Resources'),
      'required'     => FALSE,
      'settings' => array(
        'file_directory' => '/assignment_planner',
        'file_extensions' => 'txt doc docx pdf xls xlsx csv png jpg gif',
        'max_filesize' => '20MB',
      ),
      'widget'       => array(
        'type'  => 'file'
      ),
    ),

     //Hidden Resources files
    'hidden_resource_files'  => array(
      'field_name'   => 'hidden_resource_files',
      'label'        => $t('Hidden Resources'),
      'required'     => FALSE,
      'settings' => array(
        'file_directory' => '/assignment_planner',
        'file_extensions' => 'txt doc docx pdf xls xlsx csv png jpg gif',
        'max_filesize' => '20MB',
      ),
      'widget'       => array(
        'type'  => 'file'
      ),
    ),

    // Add Steps
    'steps'  => array(
      'field_name'   => 'steps',
      'label'        => $t('Steps'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
      'widget'       => array(
        'type'  => 'field_collection_embed'
      ),
    ),

  );
  return $instances;
}


/**
 * Create field collection fields.
 */
function _assignment_planner_create_field_collections_fields() {
  foreach (_assignment_planner_field_collection_fields_data() as $field) {
    field_create_field($field);
  }

  foreach (_assignment_planner_field_collection_instance_data() as $instance) {
    field_create_instance($instance);
  }
}

/**
 * Create the array of information about the field collection fields instances we want to create.
 */
function _assignment_planner_field_collection_fields_data() {

  $fields = array(
    'step_title'  => array(
      'field_name'  => 'step_title',
      'type'          => 'text',
      'cardinality' => 1,
      'translatable' => FALSE,
    ),

    'step_percent'  => array(
      'field_name'  => 'step_percent',
      'type'          => 'number_integer',
      'cardinality' => 1,
      'translatable' => FALSE,
    ),

    'step_resources'  => array(
      'field_name'  => 'step_resources',
      'type'          => 'text_long',
      'cardinality' => 1,
      'translatable' => FALSE,
    ),

  ); //end of fields array
  return $fields;
}

/**
 * Create the array of information about the field collection fields instances we want to create.
 */
function _assignment_planner_field_collection_instance_data() {
  $t = get_t();
  $instances = array(
     // Step tilte
    'step_title'  => array(
      'entity_type' => 'field_collection_item',
      'field_name'   => 'step_title',
      'bundle' => 'steps',
      'label'        => $t('Step title'),
      'required'     => TRUE,
      'widget'       => array(
        'type'  => 'text_textfield'
      ),
    ),

     // Step Percent
    'step_percent'  => array(
      'entity_type' => 'field_collection_item',
      'field_name'   => 'step_percent',
      'bundle' => 'steps',
      'label'        => $t('Step percentage'),
      'required'     => TRUE,
      'widget'       => array(
        'type'  => 'number_integer'
      ),
    ),

    // Step Resources
    'step_resources'  => array(
      'entity_type' => 'field_collection_item',
      'bundle' => 'steps',
      'field_name'   => 'step_resources',
      'label'        => $t('Step resources'),
      'widget'       => array(
        'type'  => 'text_textarea'
      ),
      'settings' => array('text_processing' => 1), // Allow text processing
      'format' => 'filter_html', // <<--- Set this value to allow filtered HTML
    ),

  ); //End of instances array
  return $instances;
}

/**
 * Implements hook_uninstall().
 */
function uw_lib_assignment_planner_uninstall() {
  drupal_set_message(t('Assignment Planner has been removed from your site.'));
}

/**
 * Implements hook_disable().
 */
function uw_lib_assignment_planner_disable() {
  remove_assignment_planner();
  drupal_set_message(t('Assignment Planner has been disabled on your site.'));
}

/**
 * Removing the assignment planner content type and its contents and fields
 */
function remove_assignment_planner() {
// Remove existing content
  $query = 'SELECT nid ';
  $query .= 'FROM {node} ';
  $query .= 'WHERE {node}.type = :type ';
  $result = db_query( $query, array(':type' => 'assignment_planner') );
  $nids = array();

  foreach ( $result as $row ) {
    $nids[] = $row->nid;
  }
  node_delete_multiple( $nids );

  // Delete fields and instances
  foreach ( field_info_instances('node', 'assignment_planner') as $field_name  => $instance ) {
    field_delete_instance($instance);
  }

  // Delete content type
  node_type_delete('assignment_planner');

  // Purge field batch
  field_purge_batch(1000); // Purge delete fields data
}

/**
 * Implements hook_enable().
 */
function uw_lib_assignment_planner_enable() {
  drupal_set_message(t('Assignment Planner has been enabled on your site.'));
}

